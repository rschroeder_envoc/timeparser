﻿Parses this:

10/1
8:15-8:30 - Work Item 1
8:30-9:15 - Work Item 2
9:15-9:45 - Work Item 2
9:45-10:45 - Work Item 3
10:45-11:15 - Work Item 4
12-5 - Work Item 2

10/2
8:15-8:30 - Work Item 4
8:30-9:15 - Work Item 2
9:15-9:45 - Work Item 1
9:45-10:45 - Work Item 3
10:45-11:15 - Work Item 4
12-5 - Work Item 9

Into this:

10/1
Work Item 1 - 00:15
Work Item 2 - 06:15
Work Item 3 - 01:00
Work Item 4 - 00:30

10/2
Work Item 1 - 00:30
Work Item 2 - 00:45
Work Item 3 - 01:00
Work Item 4 - 00:45
Work Item 9 - 05:00

Morning is from 7-12:59
Afternoon is from 1-6:59