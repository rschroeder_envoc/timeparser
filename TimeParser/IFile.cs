using System.Collections.Generic;

namespace TimeParser
{
    public interface IFile
    {
        IEnumerable<string> GetAllLines();
        void WriteLines(IEnumerable<string> lines);
        void RemoveAfter(string value);
    }
}