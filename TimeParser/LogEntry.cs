using System;

namespace TimeParser
{
    public class LogEntry
    {
        public TimeSpan TimeWorked { get; set; }

        public string Description { get; set; }
    }
}