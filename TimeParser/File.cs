using System.Collections.Generic;
using System.Linq;

namespace TimeParser
{
    public class File : IFile
    {
        private readonly string filePath;

        public File(string filePath)
        {
            this.filePath = filePath;
        }

        public IEnumerable<string> GetAllLines()
        {
            return System.IO.File.ReadAllLines(filePath);
        }

        public void WriteLines(IEnumerable<string> lines)
        {
            System.IO.File.WriteAllLines(filePath, lines);
        }

        public void RemoveAfter(string value)
        {
            var lines = GetAllLines().TakeWhile(x => x != value).ToList();
            while (0 != lines.Count && string.IsNullOrEmpty(lines[lines.Count - 1]))
            {
                lines.RemoveAt(lines.Count - 1);
            }
            System.IO.File.WriteAllLines(filePath, lines);
        }
    }
}