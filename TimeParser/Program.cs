﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TimeParser
{
    public class Program
    {
        private readonly IFile file;

        public static void Main()
        {
            var path = Path.Combine(Environment.CurrentDirectory,"time.txt");
            new Program(new File(path)).Run();
        }

        public Program(IFile file)
        {
            this.file = file;
        }

        public void Run()
        {
            file.RemoveAfter("-- Parsed Result --");
            var timeEntries = file.GetAllLines().ToList();
            var days = GetDays(timeEntries);
            var lines = FormatOutput(timeEntries, days);
            file.WriteLines(lines);
        }

        private IEnumerable<Day> GetDays(IEnumerable<string> timeEntries)
        {
            var filtered = timeEntries.Where(x => !string.IsNullOrEmpty(x)).ToList();
            if (!IsDayLine(filtered.FirstOrDefault()))
            {
                filtered.Insert(0, string.Format("{0:MM/dd}", DateTime.Now));
            }

            var split = filtered.Split(IsDayLine).Skip(1).ToList();
            return split.Select(x => new Day
                                         {
                                             Description = x.First(),
                                             Entries = GetLogEntries(x.Skip(1))
                                         }).ToList();
        }

        private static bool IsDayLine(string x)
        {
            return x.Contains("/") && x.Split('-').Count() < 2;
        }

        private static IEnumerable<LogEntry> GetLogEntries(IEnumerable<string> timeEntries)
        {
            return (from x in timeEntries.Select(LogEntryParser.Parse)
                    orderby x.Description
                    group x by x.Description
                        into g
                        let totaltime = new TimeSpan(g.Sum(z => z.TimeWorked.Ticks)).RoundTo(15)
                        select new LogEntry
                                   {
                                       Description = g.Key,
                                       TimeWorked = totaltime
                                   }).ToList();
        }

        private IEnumerable<string> FormatOutput(IEnumerable<string> timeEntries, IEnumerable<Day> days)
        {
            var parsedResult = new List<string>();
            parsedResult.Add(string.Empty);
            parsedResult.Add("-- Parsed Result --");
            foreach (var day in days)
            {
                parsedResult.Add(string.Empty);
                parsedResult.Add(day.Description);
                parsedResult.AddRange(day.Entries.Select(x => string.Format("{0} - {1:hh\\:mm}", x.Description, x.TimeWorked)));
                parsedResult.Add(string.Format("Total Time: {0:hh\\:mm}", new TimeSpan(day.Entries.Sum(x => x.TimeWorked.Ticks))));
            }
            return timeEntries.Concat(parsedResult);
        }
    }
}