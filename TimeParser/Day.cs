using System.Collections.Generic;

namespace TimeParser
{
    public class Day
    {
        public string Description { get; set; }

        public IEnumerable<LogEntry> Entries { get; set; }
    }
}