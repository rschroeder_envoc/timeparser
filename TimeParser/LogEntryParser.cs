using System;
using System.Linq;

namespace TimeParser
{
    public static class LogEntryParser
    {
        public static LogEntry Parse(string entry)
        {
            var values = entry.Split('-');
            var startTime = values[0].Trim();
            var endTime = values[1].Trim();

            string description = string.Empty;
            if (values.Length == 2)
            {
                var endvalue = endTime.Split(' ');
                endTime = endvalue[0];
                if (endvalue.Length > 1)
                {
                    description = string.Join(" ", endvalue.Skip(1));
                }
            }
            else
            {
                description = values[2].Trim();
            }

            startTime = FixTime(startTime);
            endTime = FixTime(endTime);

            var log = new LogEntry
                          {
                              Description = description,
                              TimeWorked = DateTime.Parse(endTime).Subtract(DateTime.Parse(startTime))
                          };
            return log;
        }

        private static string FixTime(string startTime)
        {
            var split = startTime.Split(':');
            int hour = int.Parse(split[0]);
            int min = 0;
            if (split.Length > 1)
            {
                min = int.Parse(split[1]);
            }

            if (hour < 7)
            {
                hour = hour + 12;
            }
            return string.Format("{0}:{1}", hour, min);
        }
    }
}