using System;
using System.Collections.Generic;
using System.Linq;

namespace TimeParser
{
    public static class Extensions
    {
        public static TimeSpan RoundTo(this TimeSpan timeSpan, int n)
        {
            return TimeSpan.FromMinutes(n*Math.Ceiling(timeSpan.TotalMinutes/n));
        }

        public static IEnumerable<IEnumerable<TSource>> Split<TSource>(
            this IEnumerable<TSource> source,
            Func<TSource, bool> predicate)
        {
            var group = new List<TSource>();
            foreach (TSource item in source)
            {
                if (predicate(item))
                {
                    yield return group.AsEnumerable();
                    group = new List<TSource>();
                }
                group.Add(item);
            }
            yield return group.AsEnumerable();
        }
    }
}