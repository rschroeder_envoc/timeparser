﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TimeParser.Tests
{
    [TestClass]
    public class LogEntryParserTests
    {
        [TestClass]
        public class TheParseMethod : LogEntryParserTests
        {
            [TestMethod]
            public void ParsesHoursCorrectly()
            {
                //arrange
                string input = "1-2 - worked";

                //act
                LogEntry result = LogEntryParser.Parse(input);

                //assert
                Assert.AreEqual(result.Description,"worked");
                Assert.AreEqual(result.TimeWorked.TotalHours, 1);
            }

            [TestMethod]
            public void ParsesMinutesCorrectly()
            {
                //arrange
                string input = "1:01-1:02 - worked";

                //act
                LogEntry result = LogEntryParser.Parse(input);

                //assert
                Assert.AreEqual(result.Description, "worked");
                Assert.AreEqual(result.TimeWorked.TotalMinutes, 1);
            }

            [TestMethod]
            public void ParsesDescriptionWithOutHyphen()
            {
                //arrange
                string input = "1:01-1:02 worked";

                //act
                LogEntry result = LogEntryParser.Parse(input);

                //assert
                Assert.AreEqual(result.Description, "worked");
                Assert.AreEqual(result.TimeWorked.TotalMinutes, 1);
            }

            [TestMethod]
            public void DeterminesTimeStartingBeforeNoonAndEndingAfterNoon()
            {
                //arrange
                string input = "10:00-1:00 - worked";

                //act
                LogEntry result = LogEntryParser.Parse(input);

                //assert
                Assert.AreEqual(result.Description, "worked");
                Assert.AreEqual(result.TimeWorked.TotalHours, 3);
            }
        }
    }
}
