﻿using System.Collections.Generic;
using System.Linq;

namespace TimeParser.Tests
{
    public class FakeFile : IFile
    {
        public FakeFile()
        {
            InputLines = new List<string>();
        }

        public IList<string> InputLines { get; set; }

        public IEnumerable<string> ResultLines { get; set; }

        public IEnumerable<string> GetAllLines()
        {
            return InputLines;
        }

        public void WriteLines(IEnumerable<string> lines)
        {
            ResultLines = lines;
        }

        public void RemoveAfter(string value)
        {
            InputLines = GetAllLines().TakeWhile(x => x != value).ToList();

            while (0 != InputLines.Count && string.IsNullOrEmpty(InputLines[InputLines.Count - 1]))
            {
                InputLines.RemoveAt(InputLines.Count - 1);
            }
        }
    }
}
