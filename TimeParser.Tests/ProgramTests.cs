﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TimeParser.Tests
{
    [TestClass]
    public class ProgramTests
    {
        [TestClass]
        public class TheRunMethod : ProgramTests
        {
            [TestMethod]
            public void OutputsSimpleResults()
            {
                //arrange
                var file = new FakeFile();
                file.InputLines.Add("10/1");
                file.InputLines.Add("1-2-worked");
                var program = new Program(file);

                //act
                program.Run();

                //assert
                AssertLines(file,
                    "10/1",
                    "1-2-worked",
                    "",
                    "-- Parsed Result --",
                    "",
                    "10/1",
                    "worked - 01:00",
                    "Total Time: 01:00");
            }

            [TestMethod]
            public void GroupsByDescription()
            {
                //arrange
                var file = new FakeFile();
                file.InputLines.Add("10/1");
                file.InputLines.Add("1-2-worked");
                file.InputLines.Add("2-3-worked");
                var program = new Program(file);

                //act
                program.Run();

                //assert
                AssertLine(file, 0, "10/1");
                AssertLine(file, 1, "1-2-worked");
                AssertLine(file, 2, "2-3-worked");
                AssertLine(file, 3, "");
                AssertLine(file, 4, "-- Parsed Result --");
                AssertLine(file, 5, "");
                AssertLine(file, 6, "10/1");
                AssertLine(file, 7, "worked - 02:00");
                AssertLine(file, 8, "Total Time: 02:00");
            }

            [TestMethod]
            public void RoundsUpTo15MinIntervals()
            {
                //arrange
                var file = new FakeFile();
                file.InputLines.Add("10/1");
                file.InputLines.Add("1:01-1:02-worked");
                var program = new Program(file);

                //act
                program.Run();

                //assert
                AssertLine(file, 0, "10/1");
                AssertLine(file, 1, "1:01-1:02-worked");
                AssertLine(file, 2, "");
                AssertLine(file, 3, "-- Parsed Result --");
                AssertLine(file, 4, "");
                AssertLine(file, 5, "10/1");
                AssertLine(file, 6, "worked - 00:15");
            }

            [TestMethod]
            public void HandlesEmptyEntries()
            {
                //arrange
                var file = new FakeFile();
                file.InputLines.Add("10/1");
                file.InputLines.Add("1:01-1:02-worked");
                file.InputLines.Add("");
                var program = new Program(file);

                //act
                program.Run();

                //assert
                AssertLine(file, 0, "10/1");
                AssertLine(file, 1, "1:01-1:02-worked");
                AssertLine(file, 2, "");
                AssertLine(file, 3, "-- Parsed Result --");
                AssertLine(file, 4, "");
                AssertLine(file, 5, "10/1");
                AssertLine(file, 6, "worked - 00:15");
                AssertLine(file, 7, "Total Time: 00:15");
            }

            [TestMethod]
            public void AllowsMultipleDays()
            {
                //arrange
                var file = new FakeFile();
                file.InputLines.Add("10/1");
                file.InputLines.Add("1-2-worked");
                file.InputLines.Add("");
                file.InputLines.Add("10/2");
                file.InputLines.Add("1-2-worked");
                var program = new Program(file);

                //act
                program.Run();

                //assert
                AssertLine(file, 0, "10/1");
                AssertLine(file, 1, "1-2-worked");
                AssertLine(file, 2, "");
                AssertLine(file, 3, "10/2");
                AssertLine(file, 4, "1-2-worked");
                AssertLine(file, 5, "");
                AssertLine(file, 6, "-- Parsed Result --");
                AssertLine(file, 7, "");
                AssertLine(file, 8, "10/1");
                AssertLine(file, 9, "worked - 01:00");
                AssertLine(file, 10, "Total Time: 01:00");
                AssertLine(file, 11, "");
                AssertLine(file, 12, "10/2");
                AssertLine(file, 13, "worked - 01:00");
                AssertLine(file, 14, "Total Time: 01:00");
            }

            [TestMethod]
            public void HandlesMissingDate()
            {
                //arrange
                var file = new FakeFile();
                file.InputLines.Add("1-2-worked");
                var program = new Program(file);

                //act
                program.Run();

                //assert
                AssertLine(file, 0, "1-2-worked");
                AssertLine(file, 1, "");
                AssertLine(file, 2, "-- Parsed Result --");
                AssertLine(file, 3, "");
                AssertLine(file, 4, string.Format("{0:MM/dd}", DateTime.Now));
                AssertLine(file, 5, "worked - 01:00");
                AssertLine(file, 6, "Total Time: 01:00");
            }

            [TestMethod]
            public void OverwritesResults()
            {
                //arrange
                var file = new FakeFile();
                file.InputLines.Add("10/1");
                file.InputLines.Add("1-2-worked");
                file.InputLines.Add("");
                file.InputLines.Add("-- Parsed Result --");
                file.InputLines.Add("");
                file.InputLines.Add("10/1");
                file.InputLines.Add("worked - 01:00");
                file.InputLines.Add("Total Time: 01:00");
                var program = new Program(file);

                //act
                program.Run();

                //assert
                AssertLine(file, 0, "10/1");
                AssertLine(file, 1, "1-2-worked");
                AssertLine(file, 2, "");
                AssertLine(file, 3, "-- Parsed Result --");
                AssertLine(file, 4, "");
                AssertLine(file, 5, "10/1");
                AssertLine(file, 6, "worked - 01:00");
                AssertLine(file, 7, "Total Time: 01:00");
            }
        }

        private void AssertLine(FakeFile file, int index, string expected)
        {
            Assert.AreEqual(expected, file.ResultLines.ElementAt(index));
        }

        private void AssertLines(FakeFile file, params string[] expected)
        {
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], file.ResultLines.ElementAt(i));
            }
        }
    }
}
